#!/bin/bash

set -eu

artisan='sudo -u www-data -i -- php /app/code/artisan'

echo "=> Run cron job"
$artisan schedule:run >> /dev/null 2>&1
