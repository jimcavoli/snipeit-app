FROM cloudron/base:2.0.0

ARG VERSION=4.9.3

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get install -y php libapache2-mod-php crudini \
    php-bcmath \
    php-curl \
    php-gd \
    php-json \
    php-ldap \
    php-mbstring \
    php-mysql \
    php-tokenizer \
    php-xml \
    php-zip \
    apache2-dev \
    build-essential \
    && rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

# Configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
COPY apache/snipe-it.conf /etc/apache2/sites-enabled/snipe-it.conf
RUN echo "Listen 3000" > /etc/apache2/ports.conf
RUN a2enmod rewrite headers

# Configure mod_php
RUN crudini --set /etc/php/7.4/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.save_path /run/sessions && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_divisor 100

# Clone and install snipe-it
RUN curl -L https://github.com/snipe/snipe-it/archive/v${VERSION}.tar.gz | tar -xz --strip-components 1 -f - && \
    mkdir -p /app/data && \
    ln -sf /app/data/.env /app/code/.env && \
    mkdir -p /app/data/uploads && rm -rf /app/code/public/uploads && ln -sf /app/data/uploads /app/code/public/uploads && \
    mkdir -p /run/sessions && \
    chown -R www-data:www-data /app/code /run/sessions

RUN gosu www-data:www-data composer install --no-dev -o && \
    mv /app/code/storage /app/data/storage && ln -sf /app/data/storage /app/code/storage

# Set up package scripts
COPY start.sh cron.sh /app/pkg/

# Copy in the special CloudronSettings additional artisan task
COPY --chown=www-data:www-data CloudronLdap.php CloudronSetup.php /app/code/app/Console/Commands/

# Lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

# Make cloudron exec sane
WORKDIR /app/data

CMD [ "/app/pkg/start.sh" ]
