#!/bin/bash

set -eu

mkdir -p /run/apache2 \
         /run/sessions

if [[ ! -f /app/data/.env ]]; then
  echo "==> Copying default configuration"
  cp /app/code/.env.example /app/data/.env
fi

echo "==> Configuring Snipe-IT"
mv /app/data/.env /app/data/env
sed -e "s/DB_CONNECTION=.*/DB_CONNECTION=mysql/" \
    -e "s/DB_HOST=.*/DB_HOST=${CLOUDRON_MYSQL_HOST}/" \
    -e "s/DB_DATABASE=.*/DB_DATABASE=${CLOUDRON_MYSQL_DATABASE}/" \
    -e "s/DB_USERNAME=.*/DB_USERNAME=${CLOUDRON_MYSQL_USERNAME}/" \
    -e "s/DB_PASSWORD=.*/DB_PASSWORD=${CLOUDRON_MYSQL_PASSWORD}/" \
    -e "s/MAIL_DRIVER=.*/MAIL_DRIVER=smtp/" \
    -e "s/MAIL_HOST=.*/MAIL_HOST=${CLOUDRON_MAIL_SMTP_SERVER}/" \
    -e "s/MAIL_PORT=.*/MAIL_PORT=${CLOUDRON_MAIL_SMTP_PORT}/" \
    -e "s/MAIL_USERNAME=.*/MAIL_USERNAME=${CLOUDRON_MAIL_SMTP_USERNAME}/" \
    -e "s/MAIL_PASSWORD=.*/MAIL_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}/" \
    -e "s/MAIL_FROM_ADDR=.*/MAIL_FROM_ADDR=${CLOUDRON_MAIL_FROM}/" \
    -e "s/MAIL_REPLYTO_ADDR=.*/MAIL_REPLYTO_ADDR=${CLOUDRON_MAIL_FROM}/" \
    -e "s/IMAGE_LIB=.*/IMAGE_LIB=gd/" \
    -e "s,APP_URL=.*,APP_URL=${CLOUDRON_APP_ORIGIN}," \
    -e "s/APP_TRUSTED_PROXIES=.*/APP_TRUSTED_PROXIES=127.0.0.1,${CLOUDRON_PROXY_IP}/" \
    -e "s/ENCRYPT=.*/ENCRYPT=true/" \
    -e "s/COOKIE_NAME=.*/COOKIE_NAME=${CLOUDRON_APP_HOSTNAME}/" \
    -e "s/COOKIE_DOMAIN=.*/COOKIE_DOMAIN=${CLOUDRON_APP_DOMAIN}/" \
    -e "s/SECURE_COOKIES=.*/SECURE_COOKIES=true/" \
    -e "s/ENABLE_CSP=.*/ENABLE_CSP=true/" \
    -e "s/REDIS_HOST=.*/REDIS_HOST=${CLOUDRON_REDIS_HOST}/" \
    -e "s/REDIS_PASSWORD=.*/REDIS_PASSWORD=${CLOUDRON_REDIS_PASSWORD}/" \
    -e "s/REDIS_PORT=.*/REDIS_PORT=${CLOUDRON_REDIS_PORT}/" \
    /app/data/env > /app/data/.env && rm -f /app/data/env

if [[ ! -f /app/data/.setup-complete ]]; then
  echo "==> Running first-time setup"

  # Temporarily turn off the ENCRYPT setting to allow the key to be generated
  mv /app/data/.env /app/data/env && \
  sed -e "s/ENCRYPT=.*/ENCRYPT=false/" /app/data/env > /app/data/.env && \
  rm -f /app/data/env

  # Generate the encryption key
  php /app/code/artisan key:generate --force

  # Re-enable ENCRYPT setting
  mv /app/data/.env /app/data/env && \
  sed -e "s/ENCRYPT=.*/ENCRYPT=true/" /app/data/env > /app/data/.env && \
  rm -f /app/data/env

  # Install the migrations table and run all migrations
  php /app/code/artisan migrate:install
  php /app/code/artisan migrate --force

  # Set up the admin user
  php /app/code/artisan snipeit:create-admin \
    --first_name "Snipe-IT" \
    --last_name "Admin" \
    --email="${CLOUDRON_MAIL_FROM}" \
    --username "admin" \
    --password "ChangeMe!"
  
  # Set default settings in the database
  php /app/code/artisan snipeit:cloudron-setup

  # Signal first-time setup complete
  touch /app/data/.setup-complete
fi

echo "=> Migrating database"
php /app/code/artisan migrate --force

echo "=> Configuring Cloudron LDAP"
php /app/code/artisan snipeit:cloudron-ldap

echo "=> Changing permissions"
chown -R www-data:www-data /app/data /run

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
